This is a read me file for my lisp Project

Troy Alcaraz

The project is to create a simple game of tic tac toe using lisp. First it will print a
blank board for two users to play on. Later it will get more complex to allow a user to 
play a computer and finally to watch two computers play each other. To put a move on the 
board enter an integer between 0 and 8, these represent the index of the square on the 
board. If the user enters an illegal move it will ask that play to input another move 
until it is valid. However is a user accidentally types in anything except a number the 
game ends and they will have to retype the specific play-game function. X's always start
with the first move.

Example game board with the indices as a visual aid:

=============
| 0 | 1 | 2 |
| 3 | 4 | 5 |
| 6 | 7 | 8 |
=============

To play a game of tic tac toe player vs. player simply open up CCL and enter the 
following functions one at a time.

Functions for player vs. player:

(load "~/directoryTottt.lispfile")

(defvar brd '(- - - - - - - - -))

(play-game-pvp brd)

To play a game of tic tac toe player vs computer do the following instructions one at a
time.

Functions for player vs computer:

(load "~/directoryTottt.lispfile")

(defvar brd '(- - - - - - - - -))

(play-game-pvc brd)

Functions for computer vs. player:

(load "~/directoryTottt.lispfile")

(defvar brd '(- - - - - - - - -))

(play-game-cvp brd)

To watch the computer play a game of tic tac toe do the following instructions on at a
time.

(load "~/directoryTottt.lispfile")

(defvar brd '(- - - - - - - - -))

(play-game-cvc brd)
