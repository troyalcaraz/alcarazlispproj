;; ttt.lisp
;;
;; Functions to help play a game of tic-tac-toe
;; Erik Steinmetz
;; Troy Alcaraz
;;

;; Just here as a handy board to test, in general don't use globals
(defparameter *brd* (list '- 'x '- 'o 'o 'o 'x '- '-))   ;; also setf

;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) )
      ((= i 9) nil)
      (if (= (mod i 3) 0) 
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; Tests whether all three items in a list are equal to each other
;; Param: alist - a list containing exactly three elements
(defun threequal (alist)
  (and (equal (first alist) (second alist))
       (equal (second alist) (third alist)))
)

;; Tests if three things in a list are equal and are not a dash
;; Param: alist - a list containing exactly three elements
(defun winList (alist)
	(and (threequal alist) (not (eql (car alist) '-)))
)

;; Grabs the nth row of a tic-tac-toe board as a list
;; Param: board - a list containing elements of a ttt board in row-major order
;; Param: row - a list of three containing elements of a ttt board
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
  )
)

;; Grabs the nth column of a tic-tac-toe board as a list
;; Param: board - a list containing elements of a ttt board in row-major order
;; Param: col - a list of three containing elements of a ttt board
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board))
)
  
;; Grabs the 1st, 5th, and 9th elements of a tic-tac-toe board as a list
;; Param: board - a list containing elements of a ttt board in row-major order
(defun grab-backslash (board)
	(list (nth 0 board) (nth 4 board) (nth 8 board))
)

;; Grabs the 3rd, 5th, and 7th elements of a tic-tac-toe board as a list
;; Param: board - a list containing elements of a ttt board in row-major order
(defun grab-forwardslash (board)
	(list (nth 2 board) (nth 4 board) (nth 6 board))
)

;; Places an x or o on the board if it is a legal move
;; Param: square - an int from 0 to 8 representing one "square" on the board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun move (square board)
	(isLegal square board)
)

;; Sets the nth element in a list to the input
;; Param: index - the index of the manipulated element
;; Param: board - a list containing elements of a tt board in row-major order
;; Param: input - an "x" or an "o"
(defun setnth (index board input)
    (if (eql index 0)
        (cons input (cdr board))
       (cons (car board) (setnth (- index 1) (cdr board) input))
    )
)

;; Tests to see if a move is legal or not
;; Param: square - an integer representing a "square" on the ttt board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun isLegal (square board)
		(and(and (>= square 0) (<= square 8)) (eql (nth square board) '-))
)

;; Generates a random move
;; Param: board - a list containing elements of a ttt board in row major order
(defun gen-random (board)
	(let ((square (random 9)))
		(if (isLegal square board)
			square
			(gen-random board)
		)
	)
)

;; Tests if someone won on the board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun isWin (board)
	(or (isVerticalWin board)
		(isHorizontalWin board)
		(isDiagonalWin board)
	)
)

;; Tests if there is a horizontal win on a board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun isHorizontalWin (board)
	(or (winList (grab-row board 0)) 
		(winList (grab-row board 1)) 
		(winList (grab-row board 2))
	)
)

;; Tests if there is a vertical win on a board
;; Param board - a list containing elements of a ttt board in row-major order
(defun isVerticalWin (board)
	(or (winList (grab-col board 0))
		(winList (grab-col board 1))
		(winList (grab-col board 2))
	)
)

;; Tests if there is a diagonal win on a board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun isDiagonalWin (board)
	(or (winList (grab-backslash board))
		(winList (grab-forwardslash board))
	)
)

;; Counts the number of moves on the board
;; Param: board - a list containing elements of a ttt board in row-major order
(defun count-moves (board)
	(if (null board)
		0
		(if (eql (car board) '-)
			(+ 0 (count-moves (cdr board)))
			(+ 1 (count-moves (cdr board)))
		)
	)
)

;; Prints out which players turn it is
;; Param: board - a list containing elements of a ttt board in a row-major order
(defun printWhosMove (board)
	(if (eql (mod (count-moves board) 2) 0)
		(princ "It's x's turn to go: ")
		(princ "It's o's turn to go: ")
	)
)
;; Outputs an "x" or an "o" to put on the ttt board
;; Param: board - a list containing elements of a ttt board in a row-major order
(defun input-x-o (board)
	(if (eql (mod (count-moves board) 2) 0)
		'x
		'o
	)
)

;; Prints out which player has won
;; Param: board - a list containing elements of a ttt board in a row major order
(defun print-who-won (board)
	(if (and (eql (mod (count-moves board) 2) 0) (isWin board))
		(print "O wins!")
		(print "X wins!")
	) 
)

;; Checks to see if the result of the game is a tie or not
;; Param: board - a list containing elements of a ttt board in a row major order
(defun isTie (board)
	(and (not (isWin board)) (eql (count-moves board) 9))
)

;; Prints out if it is a tie or not
;; Param: board - a list containing elements of a ttt board in a row major order
(defun print-tie (board)
	(if (isTie board)
		(princ "It is a tie!")
		()
	)
)

;; The driver for the player vs. computer first ttt game
;; Param: board - a list containing elements of a ttt board in row-major order
(defun pvc-driver (board)
	(princ "User's turn to go ")
	(let ((square (read)))
		(if (move square board)
			(setf board (setnth square board (input-x-o board)))
			()
		)
		(if (isWin board)
			(print-who-won board)
			(if (isTie board)
				(print-tie board)
				(setf board (setnth (gen-random board) board (input-x-o board)))
			)
		)
		
		(if (isWin board)
			(print-who-won board)
			(if (isTie board)
				(print-tie board)
				(play-game-pvc board)
			)
		)
	)
)

;; The driver for the computer vs. player first ttt game
;; Param: board - a list containing elements of a ttt board in row-major order
(defun cvp-driver (board)
	(setf board (setnth (gen-random board) board (input-x-o board)))
	(print-board board)
	(terpri)
	(princ "User's turn to go ")
	(let ((square (read)))
		(if (isWin board)
			(print-who-won board)
			(if (move square board)
				(setf board (setnth square board (input-x-o board)))
			)
		)
		(if (isWin board)
			(print-who-won board)
			(if (isTie board)
				(print-tie board)
				(play-game-cvp board)
			)
		)
	)
)

;; The driver for the computer vs. computer ttt game
;; Param: board - a list containing elements of a ttt board in row-major order
(defun cvc-driver (board)
	(setf board (setnth (gen-random board) board (input-x-o board)))
	(print-board board)
	(if (isWin board)
		(print-who-won board)
		(if (isTie board)
			(print-tie board)
			(play-game-cvc board)
		)
	)
)

;; The driver for the player vs. player ttt game
;; Param: board - a list containing elements of a ttt board in row-major order
(defun pvp-driver (board)
	(let ((square (read)))
		(if (move square board)
			(setf board (setnth square board (input-x-o board)))
			(play-game-pvp board)
		)
		(if (isWin board)
			(print-who-won board)
			(if (isTie board)
				(print-tie board)
				(play-game-pvp board)
			)
		)
	)
)

;; Initiates the game and recursively loops until a player wins
;; Param: board - a list containing elements of a ttt board in row-major order
(defun play-game-pvp (board)
	(print-board board)
	(terpri)
	(printWhosMove board)
	(terpri)
	(pvp-driver board)	
)

;; Initiates a game versus a computer where the user goes first
;; Param: board - a list containing elements of a ttt board in row-major order
(defun play-game-pvc (board)
	(print-board board)
	(terpri)
	(pvc-driver board)
)

;; Initiates a game versus a computer where the computer goes first
;; Param: board - a list containing elements of a ttt board in row-major order
(defun play-game-cvp (board)
	(print-board board)
	(terpri)
	(cvp-driver board)
)

;; Initiates a game for two computers to play each other
;; Param: board - a list containing elements of a ttt board in row-major order
(defun play-game-cvc (board)
	(print-board board)
	(terpri)
	(cvc-driver board)
)
	
	
	
	
	
	
	
	